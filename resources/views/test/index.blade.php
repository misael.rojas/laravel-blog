<?php //dd($articulo);
/*ciclos->
@for($i = 0; $i <= 5; $i++)
@if()

@endif
@endfor

@include()
@section()
@yield()
@extends()

*/
//dd($articulo);
?>

<!DOCTYPE html>
<html>
    <head>
        <title>{{ $articulo->title }}</title>

        <link href="{{ asset('css/watashi.css') }}" rel="stylesheet" type="text/css">

    </head>
    <body>

                <h1>{{ $articulo->title }}</h1>
<hr>
<p>
    {{ $articulo->content }}
</p>
<p>
    {{ $articulo->user->username }} | {{ $articulo->category->category }} |
    @foreach($articulo->tags as $tag)
    {{ $tag->tag }},
    @endforeach
</p>

    </body>
</html>
