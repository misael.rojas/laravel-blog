@extends('admin.template.main')
@section('title', 'Create user')
@section('content')

    {!! Form::open(['route' => 'admin.users.store', 'method' => 'POST' ]) !!}
     <br>
    <div class="row">
    <form class="col s12">
      <div class="row">
        <div class="input-field col s6">
         {!! Form::text('username', null, ['class' => 'validate' ]) !!}
         {!! Form::label('username', 'Name') !!}
        </div>

        <div class="input-field col s6">
          {!! Form::email('email', null, ['class' => 'validate']) !!}
         {!! Form::label('email', 'Email') !!}
        </div>

        <div class="input-field col s6">
         {!! Form::password('password', ['class' => 'validate' ]) !!}
         {!! Form::label('password', 'Password') !!}
        </div>

        <div class="input-field col s6 l6">
          {!! Form::select('user_type', [ '' => 'Select a Type',  'member' => 'Member','admin' => 'Admin' ], null ) !!}
         {!! Form::label('user_type', 'Type') !!}
        </div>
      </div>

      {!! Form::submit('Save', ['class' => 'btn waves-effect waves-light'] ) !!}
    </form>
  </div>

    {!! Form::close() !!}

@endsection


