@extends('admin.template.main')
@section('title', 'Users')
@section('content')

<br>
<div class="center-align"><a href="{{ route('admin.users.create') }}" class="waves-effect waves-light btn"><i class="material-icons">add_circle</i> Add user</a></div>

<table class="striped highlight centered">
        <thead>
          <tr>
              <th data-field="id">ID</th>
              <th data-field="name">Name</th>
              <th data-field="name">Email</th>
              <th data-field="type">Type</th>
              <th data-field="action">Action</th>
          </tr>
        </thead>

        <tbody>
         @foreach($users as $user)
          <tr>
            <td>{{ $user->user_id }}</td>
            <td>{{ $user->username }}</td>
            <td>{{ $user->email }}</td>
            <td>
                @if($user->user_type=='admin')
                    <span class="red-text">{{ $user->user_type }}</span>
                @else
                    <span class="light-blue-text">{{ $user->user_type }}</span>
                @endif
            </td>
            <td>
            <a class="btn-floating waves-effect btn-small waves-light btn red"><i class="material-icons">delete</i></a>
            <a class="btn-floating waves-effect btn-small waves-light btn yellow"><i class="material-icons">mode_edit</i></a>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
{!! $users->render() !!}

@endsection

