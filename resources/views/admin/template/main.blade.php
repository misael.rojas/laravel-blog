<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <title>@yield('title', 'Dashboard') | Admin Panel</title>
        <!--Import Google Icon Font-->
        <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!--Import materialize.css-->
        <link type="text/css" rel="stylesheet" href="{{ asset('css/materialize.min.css') }}"  media="screen,projection"/>

        <!--Let browser know website is optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <style>
            body {
                display: flex;
                min-height: 100vh;
                flex-direction: column;
            }

            main {
                flex: 1 0 auto;
            }
        </style>
    </head>

    <body>
       <header>
        @include('admin.template.nav')
        </header>

        <main>
            <div class="container">
                    <section>

                        <div class="section">
                           <div class="row">
                                <div class="col l12 m12 s12">
                                    <h3>@yield('title')</h3>
                                    <div class="divider"></div>
                                    @include('flash::message')
                                    @yield('content')
                                </div>
                            </div>

                        </div>

                    </section>


            </div>
        </main>

        <footer class="page-footer grey darken-3">
            @include('admin.template.footer')
        </footer>


      <!--Import jQuery before materialize.js-->
      <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
      <script type="text/javascript" src="{{ asset('js/materialize.min.js') }}"></script>
        <script>
            $( document ).ready(function(){
                $(".button-collapse").sideNav();

                $('select').material_select();
            })

</script>
        </script>
    </body>
</html>
