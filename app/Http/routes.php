<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'admin'], function(){
    Route::resource('users','UsersController');
});

/*
Route::group(['prefix' => 'articles'], function(){

    Route::get('view/{id}', [
        'uses' => 'testController@view',
        'as' => 'articlesView'
    ]);
});
*/
/*Route::get('articles/{nombre?}', function ($nombre = "") {
    echo "Hola $nombre esta es la seccion de articulos";
});*/
