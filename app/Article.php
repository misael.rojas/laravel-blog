<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;


class Article extends Model implements SluggableInterface
{

    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'title',
        'save_to'    => 'slug',
    ];

    protected $table = "articles";
    protected $primaryKey = 'art_id';

    protected $fillable =['title','content', 'cat_id', 'user_id'];

    public function category()
    {
        return $this->belongsTo('App\Category', 'cat_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function images()
    {
        return $this->hasMany('App\Image', 'img_id');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Tag','article_tag','art_id','tag_id')->withTimestamps();;
    }
}
