<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = "categories";
    protected $primaryKey = 'cat_id';

    protected $fillable =['category'];

    public function articles()
    {
        return $this->hasMany('App\Article', 'cat_id');
    }
}
