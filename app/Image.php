<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $table = "images";
    protected $primaryKey = 'img_id';

    protected $fillable =['image', 'art_id'];

    public function article()
    {
        return $this->belongsTo('App\Article', 'art_id');
    }
}
