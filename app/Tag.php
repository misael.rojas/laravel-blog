<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $table = "tags";
    protected $primaryKey = 'tag_id';

    protected $fillable =['tag'];

    public function articles()
    {
        return $this->belongsToMany('App\Article', 'article_tag','art_id','tag_id')->withTimestamps();
    }
}
