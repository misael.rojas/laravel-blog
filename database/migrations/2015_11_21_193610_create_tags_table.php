<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tags', function (Blueprint $table) {
            $table->increments('tag_id');
            $table->string('tag');
            $table->timestamps();
        });

        Schema::create("article_tag", function (Blueprint $table){
           $table->increments('art_tag_id');
            $table->integer('art_id')->unsigned();
            $table->integer('tag_id')->unsigned();
           $table->timestamps();

            $table->foreign('art_id')->references('art_id')->on('articles');
            $table->foreign('tag_id')->references('tag_id')->on('tags');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tags');
        Schema::drop('article_tag');
    }
}
